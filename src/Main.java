
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Création de trois client : Alexandre, Julien et Mathieu");
		Client C1 = new Client("Alexandre");
		Client C2 = new Client("Julien");
		Client C3 = new Client("Mathieu");
		System.out.println("Création d'un serveur unique S.");
		Serveur S = new Serveur();
		System.out.println("Les clients se connectent au serveur.");
		C1.seConnecter(S);
		C2.seConnecter(S);
		C3.seConnecter(S);
		System.out.println("Le client C1:Alexandre envoie un message.");
		C1.envoyer("Bonjour");
	}

}
