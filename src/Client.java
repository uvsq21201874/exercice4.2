
public class Client {
	public String nom;
	public Serveur serveur;
	
	public Client(String nom){
		this.nom=nom;
		this.serveur=null;
	}
	
	public boolean seConnecter(Serveur serveur){
		this.serveur=serveur;
		return serveur.connecter(this);
		
	}
	
	public void envoyer(String message){
		this.serveur.diffuser(message);
	}
	
	public void recevoir(String message){
		System.out.println(this.nom + " a recu : " + message);
	}
}
