import java.util.ArrayList;

public class Serveur {

	private ArrayList<Client> l = new ArrayList<Client>();
	
	public Serveur(){
		
	}
	public boolean connecter(Client client){
		this.l.add(client);
		return true;
	}
	public void diffuser(String message){
		for(Client c : l){  
			c.recevoir(message);
		} 
	}
}
